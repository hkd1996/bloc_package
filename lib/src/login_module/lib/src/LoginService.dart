

class LoginService{

  Future<User> loginWithPassword(String uname,String password) async {
    Map<String,String> userdetails=Map<String,String>();
    userdetails.putIfAbsent("testing1", () => "abcd");
    userdetails.putIfAbsent("testing2", () => "efgh");
    await Future.delayed(Duration(seconds: 2));
    if(userdetails.containsKey(uname)){
      if(userdetails[uname]==password){
        User user=new User();
        user.name=uname;
        user.age="28";
        user.department="Lottery";
        user.error=false;
        return user;
      }
      else{
        User user=new User();
        user.name="Authentication unsuccessful";
        user.error=true;
        return user;

      }
    }
    else{

      User user=new User();
      user.name="User doesnot exist";
      user.error=true;
      return user;
    }

  }

  Future<User> loginWithPin(String uname,String password) async {
    Map<String,String> userdetails=Map<String,String>();
    userdetails.putIfAbsent("testing1", () => "1234");
    userdetails.putIfAbsent("testing2", () => "4567");
    await Future.delayed(Duration(seconds: 2));
    if(userdetails.containsKey(uname)){
      if(userdetails[uname]==password){
        User user=new User();
        user.name=uname;
        user.age="28";
        user.department="Lottery";
        user.error=false;
        return user;
      }
      else{
        User user=new User();
        user.name="Authentication unsuccessful";
        user.error=true;
        return user;

      }
    }
    else{

      User user=new User();
      user.name="User doesnot exist";
      user.error=true;
      return user;
    }

  }
}

class User{
  String name;
  String age;
  String department;
  bool error;
}